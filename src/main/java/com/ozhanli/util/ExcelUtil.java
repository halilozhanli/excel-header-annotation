package com.ozhanli.util;

import com.ozhanli.annotation.ExcelHeader;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import java.io.File;
import java.io.FileOutputStream;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.UUID;

public class ExcelUtil {

    private ExcelUtil() {
    }

    public static <T> void writeToExcel(String filename, List<T> data) {
        try (XSSFWorkbook workbook = new XSSFWorkbook();
             FileOutputStream out = new FileOutputStream(new File(filename))) {
            Sheet sheet = workbook.createSheet();
            List<String> fieldNames = getFieldNamesForClass(data.get(0).getClass());
            int rowCount = 0;
            int columnCount = 0;
            Class<? extends Object> clazz = data.get(0).getClass();
            Row row = sheet.createRow(rowCount++);
            createSheetHeader(fieldNames, columnCount, clazz, row);
            createSheetRows(data, sheet, fieldNames, rowCount, clazz);
            workbook.write(out);
            out.flush();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private static void createSheetHeader(List<String> fieldNames, int columnCount, Class<?> clazz, Row row) throws NoSuchFieldException {
        for (String fieldName : fieldNames) {
            Cell cell = row.createCell(columnCount++);
            cell.setCellValue(fieldName);
            ExcelHeader header = clazz.getDeclaredField(fieldName).getAnnotation(ExcelHeader.class);
            if (Objects.nonNull(header) && !Objects.equals(header.value(), "")) {
                cell.setCellValue(header.value());
            }
        }
    }

    private static <T> void createSheetRows(List<T> data, Sheet sheet, List<String> fieldNames, int rowCount, Class<?> clazz)
            throws NoSuchMethodException, InvocationTargetException, IllegalAccessException {
        Row row;
        int columnCount;
        for (T t : data) {
            row = sheet.createRow(rowCount++);
            columnCount = 0;
            for (String fieldName : fieldNames) {
                Cell cell = row.createCell(columnCount);
                Method method;
                try {
                    method = clazz.getMethod("get" + capitalize(fieldName));
                } catch (NoSuchMethodException nme) {
                    method = clazz.getMethod("get" + fieldName);
                }
                setCellValue(t, cell, method);
                columnCount++;
            }
        }
    }

    private static <T> void setCellValue(T t, Cell cell, Method method) throws IllegalAccessException, InvocationTargetException {
        Object value = method.invoke(t, (Object[]) null);
        if (value != null) {
            if (value instanceof String) {
                cell.setCellValue((String) value);
            } else if (value instanceof Long) {
                cell.setCellValue((Long) value);
            } else if (value instanceof Integer) {
                cell.setCellValue((Integer) value);
            } else if (value instanceof Double) {
                cell.setCellValue((Double) value);
            } else if (value instanceof UUID) {
                cell.setCellValue(String.valueOf(value));
            }
        }
    }

    private static List<String> getFieldNamesForClass(Class<?> clazz) {
        List<String> fieldNames = new ArrayList<>();
        Field[] fields = clazz.getDeclaredFields();
        for (Field field : fields) {
            fieldNames.add(field.getName());
        }
        return fieldNames;
    }

    private static String capitalize(String s) {
        if (s.length() == 0)
            return s;
        return s.substring(0, 1).toUpperCase() + s.substring(1);
    }

}