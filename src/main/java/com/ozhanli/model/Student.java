package com.ozhanli.model;

import com.ozhanli.annotation.ExcelHeader;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.NonNull;
import lombok.Setter;
import lombok.experimental.FieldDefaults;

@Getter
@Setter
@NoArgsConstructor
@FieldDefaults(level = AccessLevel.PRIVATE)
public class Student {

    @ExcelHeader("Excel Header Student Id")
    int id;

    @NonNull
    @ExcelHeader("Excel Header Student name")
    String name;

    @ExcelHeader("Excel Header Student Id")
    int age;

    public Student(int id, String name, int age) {
        this.id = id;
        this.name = name;
        this.age = age;
    }

}
