package com.ozhanli.model;

import com.ozhanli.annotation.ExcelHeader;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.NonNull;
import lombok.Setter;
import lombok.experimental.FieldDefaults;

@Getter
@Setter
@NoArgsConstructor
@FieldDefaults(level = AccessLevel.PRIVATE)
public class User {

    @ExcelHeader("Excel Header User Id")
    String id;

    @NonNull
    @ExcelHeader("Excel Header User name")
    String name;

    String password;

    public User(String id, String name, String password) {
        this.id = id;
        this.name = name;
        this.password = password;
    }

}
