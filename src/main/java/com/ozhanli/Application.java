package com.ozhanli;

import com.ozhanli.model.Student;
import com.ozhanli.model.User;
import com.ozhanli.util.ExcelUtil;

import java.util.Arrays;
import java.util.List;

public class Application {

    public static void main(String[] args) {

        final List<User> users = Arrays.asList(
                new User("A", "A", "A"),
                new User("B", "B", "B"),
                new User("C", "C", "C"),
                new User("D", "D", "D"),
                new User("E", "E", "E")
        );

        final List<Student> students = Arrays.asList(
                new Student(1, "Student A", 14),
                new Student(2, "Student B", 14),
                new Student(3, "Student C", 14),
                new Student(4, "Student D", 14)
        );

        ExcelUtil.writeToExcel("Users.xlsx", users);
        ExcelUtil.writeToExcel("Students.xlsx", students);
    }
}
